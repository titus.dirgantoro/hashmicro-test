from django.shortcuts import render, redirect
from .models import Schedule
import datetime

# Create your views here.
def listSchedule(request):
    datas = Schedule.objects.all().order_by("tanggal")
    return render(
        request,
        "listschedule.html",
        {"datas": datas},
    )

def detailSchedule(request, id_aktifitas):
    data = Schedule.objects.get(id_aktifitas=id_aktifitas)
    return render(request, "detailschedule.html", {"data": data})


def addSchedulePage(request):
    return render(request, "addschedule.html",)


def addSchedule(request):
    urlrouting = "schedule:listschedule"
    if request.method == "POST":
        try:
            newSchedule = Schedule(
                nama_aktifitas=request.POST["nama_aktifitas"],
                tempat=request.POST["tempat"],
                deskripsi=request.POST["deskripsi"],
                tanggal=request.POST["tanggal"],
                status=request.POST["status"],
                created = datetime.date.today())
            newSchedule.save()
            message = "Berhasil menyimpan data"
            return render(
                request,
                "notificationsuccess.html",
                {"message": message, "urlrouting": urlrouting},
            )
        except Exception as e:
            print(e)
            message = (
                "Maaf ada beberapa kesalahan yang terdeteksi, Mohon cek ulang data"
            )
            return render(
                request,
                "notificationfailed.html",
                {"message": message, "urlrouting": urlrouting},
            )


def updateSchedulePage(request, id_aktifitas):
    data = Schedule.objects.get(id_aktifitas=id_aktifitas)
    return render(
        request, "updateschedule.html", {"data": data}
    )


def updateSchedule(request, id_aktifitas):
    urlrouting = "schedule:listschedule"
    try:
        updatedAktifitas = Schedule.objects.filter(
            id_aktifitas=id_aktifitas
        ).update(
            nama_aktifitas=request.POST["nama_aktifitas"],
            tempat=request.POST["tempat"],
            deskripsi=request.POST["deskripsi"],
            tanggal=request.POST["tanggal"],
            status=request.POST["status"],
            created = datetime.date.today()
        )
        message = "Berhasil menyimpan data"
        return render(
            request,
            "notificationsuccess.html",
            {"message": message, "urlrouting": urlrouting},
        )
    except Exception as e:
        print(e)
        message = "Maaf ada beberapa kesalahan yang terdeteksi, Mohon cek ulang data"
        return render(
            request,
            "notificationfailed.html",
            {
                "message": message,
                "urlrouting": urlrouting,
            },
        )


def deleteSchedule(request, id_aktifitas):
    urlrouting = "schedule:listschedule"
    try:
        Schedule.objects.filter(id_aktifitas=id_aktifitas).delete()
        message = "Berhasil menghapus data"
        return render(
            request,
            "notificationsuccess.html",
            {"message": message, "urlrouting": urlrouting},
        )
    except:
        message = "Maaf ada beberapa kesalahan yang terdeteksi, Mohon cek ulang data"
        return render(
            request,
            "notificationfailed.html",
            {"message": message, "urlrouting": urlrouting},
        )